<?php 
/*
	Template Name: Index.Php
*/
 ?>


<?php get_header(); ?>

<?php include (locate_template("modules/home.php" )); ?>

<?php include (locate_template("modules/nav.php" )); ?>

<?php include (locate_template("modules/menu.php" )); ?>

<?php include (locate_template("modules/instagram.php" )); ?>

<?php include (locate_template("modules/footer.php" )); ?>

<?php get_footer(); ?>