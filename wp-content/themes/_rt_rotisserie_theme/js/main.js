;(function ( $, window, document, undefined ) {

	$(document).ready(function(){





		// sticky nav redo

		var StickyNav = {

			container : $('.nav'),
			menu : $('.nav-menu'),
			navIcon : $('.nav-mobile-hamburger'),

			// variables:

			// on load: where is the nav?
			navStart : undefined,
			// on scroll: enable/disable hooks
			allowScrollHooks : true,

			_init : function(){

				$(window).on('resize load scroll', StickyNav._resizeLoadScrollHandler);
				StickyNav.navIcon.on('click', StickyNav._clickHandler);

			},
			_resizeLoadScrollHandler : function(e){

				// LOAD
				if (e.type == 'load') {
					// define the offset.top of the nav
					StickyNav.navStart = StickyNav.container.offset().top;
				}

				// SCROLL
				if (e.type == 'scroll') {
					// stick / unstick nav @ navStart
						if ( $(window).scrollTop() >= StickyNav.navStart ) {
							StickyNav.container.addClass('nav--sticky');
						} else {
							StickyNav.container.removeClass('nav--sticky');
						}

				}

			},
			_clickHandler : function(e) {
				// if the nav is open, close it,
				if (StickyNav.menu.hasClass('nav--visible')) {
					StickyNav._close();
				}
				// if the nav is closed, open it,
				else {
					StickyNav._open();
					// and then animate down:
						$('html, body').animate( {
							// scrollTop : $('.main').offset().top
							scrollTop : StickyNav.navStart
						}, 400, function(){
								StickyNav.allowScrollHooks = false;
							});
				}
			},

			// open the nav menu
			_open : function(){
				// toggle the navIcon
				StickyNav.navIcon.removeClass('ion-navicon-round')
				StickyNav.navIcon.addClass('ion-close')

				// toggle the 'visible' class
				StickyNav.menu.addClass('nav--visible');
				// animate with jQuery
				$(StickyNav.menu).slideDown();
			},
			// close the nav menu
			_close : function(){
				// toggle the navIcon
				StickyNav.navIcon.removeClass('ion-close')
				StickyNav.navIcon.addClass('ion-navicon-round')

				// toggle the 'visible' class
				StickyNav.menu.removeClass('nav--visible');
				// animate with jQuery
				$(StickyNav.menu).slideUp(400, function(){
					// remove 'display: none' from inline-styling
					// prevents auto-collapse of desktop-nav at the breakpoint
					$(StickyNav.menu).css('display', '');
				});

			},
		};
		// StickyNav._init();

























		/*
			Sticky-Top Nav
			Work in Progress;
				very extensible for other projects *clap*
		*/
		var MobileNav = {

			// $vars
			toggle : $('.nav-mobile'),
			hamburger : $('.nav-mobile-hamburger'),
			nav : $('.nav'),
			menu : $('.nav-menu'),
			downArrow : $('.home-downArrow'),

			isMobile : null,
			navStart : undefined,
			isScrollingToNav : false,

			_init : function(){
				// set "isMobile" variable:
				MobileNav._mobileCheck();
				// set 'navStart' variable:
				MobileNav.navStart = MobileNav.nav.offset().top;

				// click event for 'nav-mobile'
				MobileNav.toggle.on('click', MobileNav._clickHandler);

				// click event for 'downArrow'
				MobileNav.downArrow.on('click', MobileNav._clickHandler);
				
				// resize, load or scroll events
				$(window).on('resize load scroll', MobileNav._resizeLoadScrollHandler);
			},
			_mobileCheck : function(){
				if($(window).width() > 1024){
					MobileNav.isMobile = false;
				}
				else{
					MobileNav.isMobile = true;
				}
			},
			_clickHandler : function(){
				// whenever you 'click': 

				// if the nav is CLOSED:
				if(MobileNav.nav.hasClass('nav--visible') == false){
					if( $(window).scrollTop() <= MobileNav.navStart ){
						MobileNav._scrollToNavStart();											
					}

					if (MobileNav.isMobile == true) {
						MobileNav._open();
					}
				}

				else {
					MobileNav._close();
				}
			},
			_scrollToNavStart : function(){
				// you 'are scrolling' until the click event ends:
				MobileNav.isScrollingToNav = true;
				// scroll until the nav fixes to the top:
				$('html, body').animate( {
					scrollTop : MobileNav.navStart
				}, 400, function(){
					MobileNav.isScrollingToNav = false;
				});
			},
			_open : function(){
				MobileNav.nav.addClass('nav--visible');
				MobileNav.hamburger.removeClass('ion-navicon-round');
				MobileNav.hamburger.addClass('ion-close');
				// this leaves display:none inline-style (working as intended)
				MobileNav.menu.slideDown();
			},
			_close : function(){
				MobileNav.nav.removeClass('nav--visible');
				MobileNav.hamburger.removeClass('ion-close');
				MobileNav.hamburger.addClass('ion-navicon-round');
				
				MobileNav.menu.slideUp(400, function(){
					// clean up inline-style after, squash bugs
					MobileNav.menu.css('display', '');
				});
			},
			_resizeLoadScrollHandler : function(e){				
				
				MobileNav._mobileCheck();

				// auto-close nav if it's above nav start, otherwise make it sticky
				if ( $(window).scrollTop() < MobileNav.navStart ) {
					if( MobileNav.isScrollingToNav == false ){
						MobileNav.nav.removeClass('nav--sticky');
						if (MobileNav.nav.hasClass('nav--visible')){
							MobileNav._close();
						}
					}
				} else {
					MobileNav.nav.addClass('nav--sticky');
				}
			},
		};
		MobileNav._init();


	/*
		ScrollToAnchor (to anchor)
	*/
		var ScrollToAnchor = {
			links: $('.nav-menu-item'),
			anchors: $('.scrollAnchor'),
			offset: 40,
			_init : function(){
				ScrollToAnchor.links.on('click', ScrollToAnchor._clickHandler);
			},
			_clickHandler : function(e){

				var linkAnchor = $(this).index();

				$('html, body').animate({
					scrollTop: $(ScrollToAnchor.anchors[linkAnchor]).offset().top - ScrollToAnchor.offset,
				});
				if (MobileNav.nav.hasClass('nav--visible')){
					MobileNav._close();
				}
			},
		}
		ScrollToAnchor._init();

	/*
		Lightbox Gallery
			See _simplelightbox.scss
			https://www.andrerinas.de/simplelightbox.html
	*/
		var lightbox = $('.instagram-featured-wrapper a').simpleLightbox({

			// options:
			// use project-relevant nav-icons (ionicons):
			closeText : '<i class="ion-close"></i>',
			navText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		});

		// simple-lightbox event example;
			// see their docs
		$('.instagram-featured-wrapper a').on('error.simplelightbox', function (e) {
		  console.log(e);
		});


	/*
		End of Document Ready Function
	*/
	});
})( jQuery, window, document );

