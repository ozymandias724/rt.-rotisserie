	

<div class="main scrollAnchor">
	<div class="menu-ourmenu">
		<h1>Our Menu</h1>
	</div>

	<!-- the actual menu is 3 main sections (L C R aligned copy on comp.psd) -->
	<div class="menu">

		<!-- on desktop they want a freaking pdf ooook -->
		<div class="menu-fullMenuPDF"></div>
			
		<!-- L. SideMenu -->
		<div class="menu-leftSideModule">
			<!-- Chefs Table -->
			<div>
				<h2>Chef Picks</h2>
				<div>
					<p>95 - per person</p>
					<p>participation by the entire table is required</p>
					<p>Pairings - 75</p>
				</div>
			</div>	
			<!-- Caviar -->
			<div>
				<h2>Caviar</h2>
					
				<div>
					<p>Fried Chicken Madeleine,</p>
					<p>Tsar Nicoulai</p>
					<p>14</p>
				</div>
			</div>
			<!-- Bread and Butter -->
			<div>
				<h2>Bread &amp butter</h2>
				<div>
					<p>Douglas Fir Levain</p>
					<p>House Cultured Butter</p>
					<p>2.50</p>
					<div>						
						<p>Butter to go</p>
						<p>6</p>
						<p>Bread to go</p>
						<p>7</p>
					</div>
				</div>
			</div>
			<!-- Bites Section -->
			<div>
				<h2>Bites</h2>
				<div>	
					<p>Dried Porcini Doughnuts</p>
					<p>Raclette</p>
					<p>9</p>
				</div>
				<div>
					<p>Sardine Chip</p>
					<p>Horseradish Creme Reaiche</p>
					<p>2</p>
				</div>
				<div>
					<p>Brandade Chips</p>
					<p>Douglas Fir, Onion Dip</p>
					<p>9</p>
				</div>
				<div>
					<p>Smoked Halibut</p>
					<p>Pickled &amp Raw vegetables</p>
					<p>seeds</p>
					<p>9</p>
				</div>
				<div>
					<p>xiao long bao</p>
					<p>kale salsa verde</p>
					<p>5</p>
				</div>
				<div>
					<p>white bean garlic confit</p>
					<p>pan con tomate</p>
					<p>9</p>
				</div>
			</div>
			<!-- Oysters Section -->
			<div>
				<h2>Oysters on the 1/2</h2>
				<p>Drakes Bay - CA</p>
				<p>Bread and Butter Pickles</p>
				<p>3.75EA</p>
			</div>
		</div>

		<!-- Main Menu -->
		<div class="menu-centerModule">

			<div>
				<p>Roasted Asparagus, Tom Kha Gai, Maitake, Chili Jam</p>
				<p>16</p>
				<p>Burrata, Strawberry gazpacho, olice oil, radish, green garlic</p>
				<p>15</p>
				<p>Local Halibut, cucumber, avocado, ramp kimchi</p>
				<p>17</p>
				<p>beef tartare, roasted carrot, vaudovan, shoestring potatoes</p>
				<p>16</p>
			</div>

			<div>
				<p>spaghetti, sweetbread parm, mozzarella curd, garlic bread</p>
				<p>19</p>
				<p>aged beef agnolotti, white corn, black olive, pecorino</p>
				<p>17</p>
				<p>tajarin barigoule, crispy artichoke, mitica sardo</p>
				<p>18</p>
				<p>bbq spring lamb lo mein, ginger scallion</p>
				<p>18</p>
			</div>
			<div>
				<p>douglas fir pierogis, english pea, hazelnut, goat cheese</p>
				<p>26</p>
				<p>black cod, japanese curry, fava bean, umeboshi</p>
				<p>33</p>
				<p>dry aged ribeye, mexican corn, salsa roja, lobster butter</p>
				<p>37</p>
			</div>

			<div class="menu-disclaimer">
				<p>in response to san fransisco employee mandates a 4% surcharge will be added to all sales</p>
				<p>*consumption of raw or undercooked food could increase the risk of food borne illness</p>
			</div>
		</div>

		<!-- R. Side Menu -->
		<div class="menu-rightSideModule">
		<!-- rich table -->
			<div class="menu-logoImages scrollAnchor">
				<img src="/_rt_rotisserie/wp-content/themes/_rt_rotisserie_theme/library/img/richtable.jpg" alt="">
			</div>
			<div>
				<h3><span>Chef de Cuisine</span></h3>
				<h3><span>Brandon Rice</span></h3>
			</div>
			<div>
				<p>all of our ingredients are sourced from the best places possible</p>
				<p>if you dlike to know more</p>
				<p>just ask</p>
			</div>
			<div>
				<p>for news find us on facebook</p>
				<p>and follow us on twitter &amp instagram</p>
				<p><span>@richtable</span></p>
			</div>
			<div>
				<p>199 Gough St</p>
				<p>415-355-9085</p>
				<p><a href="#">www.richtablesf.com</a></p>
			</div>
			<div class="menu-logoImages">
				<img src="/_rt_rotisserie/wp-content/themes/_rt_rotisserie_theme/library/img/radish.jpg" alt="">
			</div>
		</div>
		
		<!-- Logo Thing -->
		<div class="olive-logo"></div>
	</div>



	
	<!-- the buttons below the menu -->
	<!-- wrapper: -->
	<div class="menu-button-wrapper">
		<!-- buttons: -->
		<div class="menu-buttonPDF">
			<p><a href="#">Click to download a PDF</a></p>
		</div>
			
		<div>
			<div class="menu-button button-Takeout">
				<h3>Takeout</h3>
			</div>
			<div class="menu-button button-Delivery">
				<h3>Delivery</h3>
			</div>
		</div>
	</div>
</div>