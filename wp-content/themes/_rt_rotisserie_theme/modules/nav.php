
<nav class="nav">
	<div class="nav-mobile">
		<i class="nav-mobile-hamburger ion-navicon-round"></i>
	</div>
	<ul class="nav-menu">
		<li class="nav-menu-item"><a class="nav-menu-item-link">Menu</a></li>
		<li class="nav-menu-item"><a class="nav-menu-item-link">Takeout</a></li>
		<li class="nav-menu-item"><a class="nav-menu-item-link">Delivery</a></li>
		<li class="nav-menu-item"><a class="nav-menu-item-link">Location</a></li>
	</ul>
</nav>