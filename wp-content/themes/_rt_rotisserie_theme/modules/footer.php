


<div class="footer-wrapper scrollAnchor">


	<div class="footer-main">

		<div class="footer-main-centerer">
		
			<div class="logo">
				<!-- <img src="" alt=""> -->
			</div>
		
			<div class="footer-contactInfo-buttons-wrapper">
				
				<div class="footer-contactInfo">
					<p>RT ROTISSERIE</p>
					<p>101 Oak Street, San Francisco, CA 94102</p>
					<p><a href="tel://415-355-9085">415-355-9085</a></p>
				</div>


				<div class="footer-buttons-wrapper">
					<div class="footer-buttons">Takeout</div>
					<div class="footer-buttons">Delivery</div>
				</div>
			</div>			
		</div>
	</div>


	<div class="footer-subFooter">
	 
		 <div>
			<p><a href="#">Visit our sister restaurant <span>Rich Table</span></a></p>
			<p><a href="#">Round White Social Media <span>Links</span></a></p>
		 </div>

	</div>

</div>

